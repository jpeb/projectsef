﻿using ProjectsEF.Common.DTOs.Task;
using System;

namespace ProjectsEF.Common.DTOs.Project
{
    public class ProjectStaticticsDTO
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO LongestTaskByDescription { get; set; }
        public TaskDTO ShortestTaskByName { get; set; }
        public int? UsersNumber { get; set; }
    }
}
