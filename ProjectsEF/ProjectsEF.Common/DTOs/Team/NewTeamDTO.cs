﻿using System;

namespace ProjectsEF.Common.DTOs.Team
{
    public class NewTeamDTO
    {
        public string Name { get; set; }
    }
}
