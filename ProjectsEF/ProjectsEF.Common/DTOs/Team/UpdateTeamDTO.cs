﻿using System.Text.Json.Serialization;

namespace ProjectsEF.Common.DTOs.Team
{
    public class UpdateTeamDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
