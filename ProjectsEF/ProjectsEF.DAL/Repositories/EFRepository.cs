﻿using ProjectsEF.DAL.Repositories.Abstract;
using ProjectsEF.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using ProjectsEF.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace ProjectsEF.DAL.Repositories
{
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ProjectsDbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public EFRepository(ProjectsDbContext context)
        {
            _context = context;
            _dbSet = _context.Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>> filter = null, bool asSplitQuery = false,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter is not null)
            {
                query = query.Where(filter);
            }

            if(include is not null)
            {
                query = include(query);
            }

            if(asSplitQuery)
                query = query.AsSplitQuery();


            return query.AsNoTracking();
        }

        public TEntity GetById(int id)
        {
            return _dbSet.SingleOrDefault(s => s.Id == id);
        }

        public TEntity Add(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            _dbSet.Add(entity);

            return entity;
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities == null) throw new ArgumentNullException(nameof(entities));

            _dbSet.AddRange(entities);
        }

        public void Update(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            _context.Entry(entity).State = EntityState.Modified;
        }
        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }
    }
}
