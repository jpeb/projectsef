﻿using ProjectsEF.DAL.Context;
using ProjectsEF.DAL.Entities;
using ProjectsEF.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = ProjectsEF.DAL.Entities.Task;

namespace ProjectsEF.DAL.Repositories
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly ProjectsDbContext _db;

        public EFUnitOfWork(ProjectsDbContext databaseContext)
        {
            _db = databaseContext;

            ProjectRepository = new EFRepository<Project>(databaseContext);
            TaskRepository = new EFRepository<Task>(databaseContext);
            TeamRepository = new EFRepository<Team>(databaseContext);
            UserRepository = new EFRepository<User>(databaseContext);
        }

        public IRepository<Project> ProjectRepository { get; }
        public IRepository<Task> TaskRepository { get; }
        public IRepository<Team> TeamRepository { get; }
        public IRepository<User> UserRepository { get; }

        public int SaveChanges()
        {
            return _db.SaveChanges();
        }

        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
