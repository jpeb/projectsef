﻿using Microsoft.EntityFrameworkCore.Query;
using ProjectsEF.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectsEF.DAL.Repositories.Abstract
{
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> GetAll(Expression<Func<T, bool>> filter = null, bool asSplitQuery = false,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);
        T GetById(int id);
        T Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Update(T entity);
        void Delete(T entity);
    }
}
