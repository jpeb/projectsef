﻿using ProjectsEF.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = ProjectsEF.DAL.Entities.Task;

namespace ProjectsEF.DAL.Repositories.Abstract
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Project> ProjectRepository { get; }
        IRepository<Task> TaskRepository { get; }
        IRepository<Team> TeamRepository { get; }
        IRepository<User> UserRepository { get; }

        int SaveChanges();
    }
}
