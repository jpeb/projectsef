using ProjectsEF.DAL.Entities.Abstract;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProjectsEF.DAL.Entities
{
    public class Team : BaseEntity
    {
        [Required]
        [MaxLength(500)]
        public string Name { get; set; }
        public List<Project> Projects { get; set; }
        public List<User> Users { get; set; }
    }
}
