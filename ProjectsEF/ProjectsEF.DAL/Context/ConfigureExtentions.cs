﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = ProjectsEF.DAL.Entities.Task;

namespace ProjectsEF.DAL.Context
{
    public static class ConfigureExtentions
    {
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>()
                .HasOne(p => p.Performer)
                .WithMany(t => t.Tasks)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
