﻿using ProjectsEF.BLL.Exceptions;
using ProjectsEF.WebAPI.Enums;
using System;
using System.Net;

namespace ProjectsEF.WebAPI.Extensions
{
    public static class ExceptionFilterExtensions
    {
        public static (HttpStatusCode statusCode, ErrorCode errorCode) ParseException(this Exception exception)
        {
            return exception switch
            {
                NotFoundException _ => (HttpStatusCode.NotFound, ErrorCode.NotFound),
                _ => (HttpStatusCode.InternalServerError, ErrorCode.General),
            };
        }
    }
}
