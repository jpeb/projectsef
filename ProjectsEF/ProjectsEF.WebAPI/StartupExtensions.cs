﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ProjectsEF.BLL.MappingProfiles;
using ProjectsEF.BLL.Services;
using ProjectsEF.BLL.Services.Abstract;
using ProjectsEF.DAL.Context;
using ProjectsEF.DAL.Repositories;
using ProjectsEF.DAL.Repositories.Abstract;
using System.Linq;
using System.Reflection;

namespace ProjectsEF.WebAPI
{
    public static class StartupExtensions
    {
        public static void RegisterRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, EFUnitOfWork>();
        }

        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IProjectsService, ProjectsService>();
            services.AddScoped<ITasksService, TasksService>();
            services.AddScoped<IUsersService, UsersService>();
            services.AddScoped<ITeamsService, TeamsService>();
        }

        public static void RegisterAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(cfg =>
            {
                cfg.AddProfile<ProjectProfile>();
                cfg.AddProfile<TaskProfile>();
                cfg.AddProfile<UserProfile>();
                cfg.AddProfile<TeamProfile>();
            },
            Assembly.GetExecutingAssembly());
        }

        public static IHost MigrateDB(this IHost host)
        {
            using (var serviceScope = host.Services.CreateScope())
            using (var context = serviceScope.ServiceProvider.GetRequiredService<ProjectsDbContext>())
            {
                context.Database.Migrate();
            }

            return host;
        }

        public static IHost SeedTestData(this IHost host, int teamsNumber, int usersNumber, int projectsNumber, int tasksNumber)
        {
            using (var serviceScope = host.Services.CreateScope())
            using (var context = serviceScope.ServiceProvider.GetRequiredService<ProjectsDbContext>())
            {
                if (!context.Users.Any())
                    context.SeedTestData(teamsNumber, usersNumber, projectsNumber, tasksNumber);
            }

            return host;
        }
    }
}
