﻿using Microsoft.AspNetCore.Mvc;
using ProjectsEF.BLL.Services.Abstract;
using System.Collections.Generic;
using System;
using System.Linq;
using ProjectsEF.Common.DTOs.User;

namespace ProjectsEF.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService _usersService;

        public UsersController(IUsersService usersService)
        {
            _usersService = usersService;
        }

        [HttpGet]
        public ActionResult<ICollection<UserDTO>> GetAll()
        {
            return _usersService.GetAll().ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<UserDTO> Get(int id)
        {
            return _usersService.Get(id);
        }
        
        [HttpGet("withTasks")]
        public ActionResult<ICollection<UserTasksDTO>> GetUsersWithTasks()
        {
            return _usersService.GetUsersWithTasks().ToList();
        }
        

        [HttpGet("{id}/taskStatictics")]
        public ActionResult<UserTaskStaticticsDTO> GetUserTaskStatictics(int id)
        {
            return _usersService.GetUserTaskStatictics(id);
        }

        [HttpPost]
        public ActionResult Add([FromBody] NewUserDTO newUser)
        {
            var createdUser = _usersService.Add(newUser);

            return Created($"users/{createdUser.Id}", createdUser);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] UpdateUserDTO newUser)
        {
            newUser.Id = id;
            _usersService.Update(newUser);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _usersService.Delete(id);

            return NoContent();
        }
    }
}
