﻿using Microsoft.AspNetCore.Mvc;
using ProjectsEF.BLL.Services.Abstract;
using System.Collections.Generic;
using System;
using System.Linq;
using ProjectsEF.Common.DTOs.Task;

namespace ProjectsEF.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private readonly ITasksService _tasksService;

        public TasksController(ITasksService tasksService)
        {
            _tasksService = tasksService;
        }

        [HttpGet]
        public ActionResult<ICollection<TaskDTO>> GetAll()
        {
            return _tasksService.GetAll().ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDTO> Get(int id)
        {
            return _tasksService.Get(id);
        }

        [HttpGet("assignedToUser/{userId}")]
        public ActionResult<ICollection<TaskDTO>> GetUserTasksWithNameLessThen(int userId, [FromQuery]int? maxTaskNameLength)
        {
            maxTaskNameLength ??= int.MaxValue;

            return _tasksService.GetUserTasksWithNameLessThen(userId, maxTaskNameLength.Value).ToList();
        }

        [HttpGet("finishedInYear/{finishedYear}")]
        public ActionResult<ICollection<TaskNameDTO>> GetUserTasksFinishedInYear(int finishedYear, [FromQuery] int userId)
        {
            return _tasksService.GetUserTasksFinishedInYear(userId, finishedYear).ToList();
        }

        [HttpPost]
        public ActionResult Add([FromBody] NewTaskDTO newTask)
        {
            var createdTask = _tasksService.Add(newTask);

            return Created($"tasks/{createdTask.Id}", createdTask);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] UpdateTaskDTO newTask)
        {
            newTask.Id = id;
            _tasksService.Update(newTask);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _tasksService.Delete(id);

            return NoContent();
        }
    }
}
