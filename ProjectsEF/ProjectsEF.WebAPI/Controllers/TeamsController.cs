﻿using Microsoft.AspNetCore.Mvc;
using ProjectsEF.BLL.Services.Abstract;
using System.Collections.Generic;
using System;
using System.Linq;
using ProjectsEF.Common.DTOs.Team;

namespace ProjectsEF.WebAPI.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamsService _teamsService;

        public TeamsController(ITeamsService teamsService)
        {
            _teamsService = teamsService;
        }

        [HttpGet]
        public ActionResult<ICollection<TeamDTO>> GetAll()
        {
            return _teamsService.GetAll().ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<TeamDTO> Get(int id)
        {
            return _teamsService.Get(id);
        }

        [HttpGet("withUsersOlderThan/{minYearsNumber}")]
        public ActionResult<ICollection<TeamUsersDTO>> GetTeamUsersOlderThan(int minYearsNumber)
        {
            return _teamsService.GetTeamUsersOlderThan(minYearsNumber).ToList();
        }

        [HttpPost]
        public ActionResult Add([FromBody] NewTeamDTO newTeam)
        {
            var createdTeam = _teamsService.Add(newTeam);

            return Created($"teams/{createdTeam.Id}", createdTeam);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] UpdateTeamDTO newTeam)
        {
            newTeam.Id = id;
            _teamsService.Update(newTeam);
                        
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _teamsService.Delete(id);

            return NoContent();
        }
    }
}
