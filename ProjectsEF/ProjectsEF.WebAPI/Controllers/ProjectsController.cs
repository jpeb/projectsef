﻿using Microsoft.AspNetCore.Mvc;
using ProjectsEF.BLL.Services.Abstract;
using ProjectsEF.Common.DTOs.Project;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectsEF.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectsService _projectsService;

        public ProjectsController(IProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        [HttpGet]
        public ActionResult<ICollection<ProjectDTO>> GetAll()
        {
            return _projectsService.GetAll().ToList();
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDTO> Get(int id)
        {
            return _projectsService.Get(id);
        }

        [HttpGet("tasksNumber/{userId}")]
        public ActionResult<ICollection<ProjectTasksNumberDTO>> GetUserProjectsTasksNumber(int userId)
        {
            return _projectsService.GetUserProjectsTasksNumber(userId).ToList();
        }

        [HttpGet("statictics")]
        public ActionResult<ICollection<ProjectStaticticsDTO>> GetProjectStatictics()
        {
            return _projectsService.GetProjectStatictics(20, 3).ToList();
        }
        

       [HttpPost]
        public ActionResult Add([FromBody] NewProjectDTO newProject)
        {
            var createdProject = _projectsService.Add(newProject);

            return Created($"projects/{createdProject.Id}", createdProject);
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] UpdateProjectDTO newProject)
        {
            newProject.Id = id;
            _projectsService.Update(newProject);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            _projectsService.Delete(id);

            return NoContent();
        }
    }
}
