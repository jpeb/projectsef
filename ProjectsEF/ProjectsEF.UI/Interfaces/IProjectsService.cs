﻿using ProjectsEF.Common.DTOs.Project;
using ProjectsEF.Common.DTOs.Task;
using ProjectsEF.Common.DTOs.Team;
using ProjectsEF.Common.DTOs.User;
using System;
using System.Collections.Generic;

namespace ProjectsEF.UI.Interfaces
{
    public interface IProjectsService : IDisposable
    {
        // Query 1
        ICollection<ProjectTasksNumberDTO> GetUserProjectTasksNumber(int userId);

        // Query 2
        ICollection<TaskDTO> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength);

        // Query 3
        ICollection<TaskNameDTO> GetUserTasksFinishedInYear(int userId, int finishedYear);

        // Query 4
        ICollection<TeamUsersDTO> GetTeamsWithUsersOlderThan(int minYearsNumber);

        // Query 5
        ICollection<UserTasksDTO> GetUsersWithTasks();

        // Query 6
        UserTaskStaticticsDTO GetUserTaskStatictics(int userId);

        // Query 7
        ICollection<ProjectStaticticsDTO> GetProjectStatictics();
    }
}