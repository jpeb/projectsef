﻿using ProjectsEF.UI.Configuration;
using ProjectsEF.UI.Interfaces;
using ProjectsEF.UI.Menu;
using ProjectsEF.UI.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace ProjectsEF.UI
{
    internal class Program
    {
        private const string Title = @"
  _____       _   _ _           _____                                            _    
 | ____|_ __ | |_(_) |_ _   _  |  ___| __ __ _ _ __ ___   _____      _____  _ __| | __
 |  _| | '_ \| __| | __| | | | | |_ | '__/ _` | '_ ` _ \ / _ \ \ /\ / / _ \| '__| |/ /
 | |___| | | | |_| | |_| |_| | |  _|| | | (_| | | | | | |  __/\ V  V / (_) | |  |   < 
 |_____|_| |_|\__|_|\__|\__, | |_|  |_|  \__,_|_| |_| |_|\___| \_/\_/ \___/|_|  |_|\_\
                        |___/                                                        

    Use arrows and Enter for navigation...                                                                                          
";

        private static void Main(string[] args)
        {
            Console.Title = "ProjectsEF.UI";
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;

            var client = new HttpClient
            {
                BaseAddress = new Uri(ApiUris.BaseAddress)
            };

            IProjectsService projectsService = new ProjectsService(client);

            var app = new AppController(
                projectsService
            );

            List<MenuOption> options = new()
            {
                new MenuOption("Query 1 - Кол-во тасков у проекта пользователя", app.Query1),
                new MenuOption("Query 2 - Список тасков пользователя с name < X", app.Query2),
                new MenuOption("Query 3 - Список выполненых тасков пользователя за X год", app.Query3),
                new MenuOption("Query 4 - Команды пользователей с участниками старше X лет", app.Query4),
                new MenuOption("Query 5 - Список пользователей и их таски", app.Query5),
                new MenuOption("Query 6 - Информация о тасках пользователя", app.Query6),
                new MenuOption("Query 7 - Информация о тасках проектов", app.Query7),
                new MenuOption("Exit", () =>
                {
                    app.Dispose();
                    Environment.Exit(0);
                })
            };

            var menu = new ConsoleMenu(options, Title)
            {
                PrimaryColor = (ConsoleColor.White, ConsoleColor.Black),
                TitleColor = (ConsoleColor.Red, ConsoleColor.Black),
                SelectedColor = (ConsoleColor.Black, ConsoleColor.DarkGreen)
            };

            menu.RunMenu();
        }
    }
}