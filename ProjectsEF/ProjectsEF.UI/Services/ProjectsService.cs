﻿using ProjectsEF.Common.DTOs.Project;
using ProjectsEF.Common.DTOs.Task;
using ProjectsEF.Common.DTOs.Team;
using ProjectsEF.Common.DTOs.User;
using ProjectsEF.UI.Configuration;
using ProjectsEF.UI.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;

namespace ProjectsEF.UI.Services
{
    public class ProjectsService : IDisposable, IProjectsService
    {
        private readonly HttpClient _httpClient;

        public ProjectsService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        // Query 1
        public ICollection<ProjectTasksNumberDTO> GetUserProjectTasksNumber(int userId)
        {
            return _httpClient.GetFromJsonAsync<ICollection<ProjectTasksNumberDTO>>(
                    ApiUris.UserProjectsTasksNumber(userId)
                ).Result;
        }

        // Query 2
        public ICollection<TaskDTO> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength)
        {
            return _httpClient.GetFromJsonAsync<ICollection<TaskDTO>>(
                    ApiUris.UserTasksWithNameLessThen(userId, maxTaskNameLength)
                ).Result;
        }

        // Query 3
        public ICollection<TaskNameDTO> GetUserTasksFinishedInYear(int userId, int finishedYear)
        {
            return _httpClient.GetFromJsonAsync<ICollection<TaskNameDTO>>(
                    ApiUris.UserTasksFinishedInYear(finishedYear, userId)
                ).Result;
        }

        // Query 4
        public ICollection<TeamUsersDTO> GetTeamsWithUsersOlderThan(int minYearsNumber)
        {
            return _httpClient.GetFromJsonAsync<ICollection<TeamUsersDTO>>(
                    ApiUris.TeamUsersOlderThan(minYearsNumber)
                ).Result;
        }


        // Query 5
        public ICollection<UserTasksDTO> GetUsersWithTasks()
        {
            return _httpClient.GetFromJsonAsync<ICollection<UserTasksDTO>>(
                    ApiUris.UsersWithTasks()
                ).Result;
        }


        // Query 6
        public UserTaskStaticticsDTO GetUserTaskStatictics(int userId)
        {
            return _httpClient.GetFromJsonAsync<UserTaskStaticticsDTO>(
                    ApiUris.UserTaskStatictics(userId)
                ).Result;
        }


        // Query 7
        public ICollection<ProjectStaticticsDTO> GetProjectStatictics()
        {
            return _httpClient.GetFromJsonAsync<ICollection<ProjectStaticticsDTO>>(
                    ApiUris.ProjectStatictics()
                ).Result;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }
    }
}