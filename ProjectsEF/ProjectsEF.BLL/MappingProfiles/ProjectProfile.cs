﻿using AutoMapper;
using ProjectsEF.Common.DTOs.Project;
using ProjectsEF.DAL.Entities;

namespace ProjectsEF.BLL.MappingProfiles
{
    public sealed class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>();

            CreateMap<NewProjectDTO, Project>();
            CreateMap<UpdateProjectDTO, Project>();
        }
    }
}
