﻿using AutoMapper;
using ProjectsEF.Common.DTOs.User;
using ProjectsEF.DAL.Entities;
using System.Linq;

namespace ProjectsEF.BLL.MappingProfiles
{
    public sealed class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(x => x.RegisteredAt, m => m.MapFrom(p => p.CreatedAt));

            CreateMap<User, UserTasksDTO>()
                .ForMember(x => x.RegisteredAt, m => m.MapFrom(p => p.CreatedAt))
                .ForMember(x => x.Tasks, m => m.MapFrom(u => u.Tasks.OrderByDescending(t => t.Name.Length)));

            CreateMap<NewUserDTO, User>();
            CreateMap<UpdateUserDTO, User>();
        }
    }
}
