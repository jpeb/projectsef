﻿using AutoMapper;
using ProjectsEF.Common.DTOs.Team;
using ProjectsEF.DAL.Entities;
using System.Linq;

namespace ProjectsEF.BLL.MappingProfiles
{
    public sealed class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>();
            CreateMap<Team, TeamUsersDTO>().ForMember(t => t.Users, p => p.MapFrom(t => t.Users.OrderByDescending(u => u.CreatedAt)));

            CreateMap<NewTeamDTO, Team>();
            CreateMap<UpdateTeamDTO, Team>();
        }
    }
}
