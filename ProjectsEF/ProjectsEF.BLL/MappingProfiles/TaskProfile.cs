﻿using AutoMapper;
using ProjectsEF.Common.DTOs.Task;
using ProjectsEF.DAL.Entities;

namespace ProjectsEF.BLL.MappingProfiles
{
    public sealed class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDTO>();
            CreateMap<Task, TaskNameDTO>();

            CreateMap<NewTaskDTO, Task>();
            CreateMap<UpdateTaskDTO, Task>();
        }
    }
}
