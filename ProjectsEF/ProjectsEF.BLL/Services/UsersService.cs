﻿using AutoMapper;
using ProjectsEF.Common.DTOs.User;
using ProjectsEF.BLL.Services.Abstract;
using ProjectsEF.DAL.Entities;
using ProjectsEF.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectsEF.Common.DTOs.Project;
using ProjectsEF.Common.DTOs.Task;
using ProjectsEF.BLL.Exceptions;

namespace ProjectsEF.BLL.Services
{
    public class UsersService : BaseService, IUsersService
    {
        private readonly IUnitOfWork _context;
        private readonly IRepository<User> _userRepository;

        public UsersService(IMapper mapper, IUnitOfWork context) : base(mapper)
        {
            _context = context;
            _userRepository = _context.UserRepository;
        }

        public IEnumerable<UserDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_userRepository.GetAll());
        }

        public UserDTO Get(int userId)
        {
            var user = _userRepository.GetById(userId);

            if (user is null) throw new NotFoundException(nameof(user), userId);

            return _mapper.Map<UserDTO>(user);
        }

        public UserDTO Add(NewUserDTO user)
        {
            var newUser = _userRepository.Add(_mapper.Map<User>(user));
            _context.SaveChanges();

            return _mapper.Map<UserDTO>(newUser);
        }

        public void Delete(int userId)
        {
            var user = _userRepository.GetById(userId);

            if (user is null) throw new NotFoundException(nameof(user), userId);

            _userRepository.Delete(user);
            _context.SaveChanges();
        }

        public void Update(UpdateUserDTO newUser)
        {
            var user = _userRepository.GetById(newUser.Id);

            if (user is null) throw new NotFoundException(nameof(user), newUser.Id);

            _mapper.Map(newUser, user);
            user.UpdatedAt = DateTime.Now;

            _userRepository.Update(user);
            _context.SaveChanges();
        }

        // Query  5
        public ICollection<UserTasksDTO> GetUsersWithTasks()
        {
            return _mapper.Map<ICollection<UserTasksDTO>>(
                _userRepository.GetAll(null, true, s => s.Include(p => p.Tasks)).OrderBy(u => u.FirstName).ToList()
                );
        }

        // Query 6
        public UserTaskStaticticsDTO GetUserTaskStatictics(int userId)
        {
            var result = from user in _userRepository.GetAll(u => u.Id == userId, true, s => s.Include(u => u.Tasks))
                         let sortedProjects = _context.ProjectRepository.GetAll(null, true, s => s.Include(u => u.Tasks)).OrderByDescending(p => p.CreatedAt)
                         let lastProject = sortedProjects.First()
                         let lastUserProject = sortedProjects.FirstOrDefault(p => p.AuthorId == userId)
                         select new UserTaskStaticticsDTO
                         {
                             User = _mapper.Map<UserDTO>(user),
                             LastUserProject = _mapper.Map<ProjectDTO>(lastUserProject),
                             AllUsersLastProjectTasksNumber = lastProject.Tasks.Count,
                             UnfinishedUserTasksNumber = user.Tasks.Count(t => t.State != TaskState.Finished),
                             LongestUserTask = _mapper.Map<TaskDTO>(user.Tasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault())
                         };

            return result.FirstOrDefault();
        }
    }
}
