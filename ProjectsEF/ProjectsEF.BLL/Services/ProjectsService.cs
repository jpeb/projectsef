﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectsEF.BLL.Exceptions;
using ProjectsEF.BLL.Services.Abstract;
using ProjectsEF.Common.DTOs.Project;
using ProjectsEF.Common.DTOs.Task;
using ProjectsEF.DAL.Entities;
using ProjectsEF.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectsEF.BLL.Services
{
    public class ProjectsService : BaseService, IProjectsService
    {
        private readonly IUnitOfWork _context;
        private readonly IRepository<Project> _projectRepository;

        public ProjectsService(IMapper mapper, IUnitOfWork context) : base(mapper)
        {
            _context = context;
            _projectRepository = _context.ProjectRepository;
        }

        public IEnumerable<ProjectDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<ProjectDTO>>(_projectRepository.GetAll());
        }

        public ProjectDTO Get(int projectId)
        {
            var project = _projectRepository.GetById(projectId);

            if (project is null) throw new NotFoundException(nameof(project), projectId);

            return _mapper.Map<ProjectDTO>(project);
        }

        public ProjectDTO Add(NewProjectDTO project)
        {
            var newProject = _projectRepository.Add(_mapper.Map<Project>(project));
            _context.SaveChanges();

            return _mapper.Map<ProjectDTO>(newProject);
        }

        public void Delete(int projectId)
        {
            var project = _projectRepository.GetById(projectId);

            if (project is null) throw new NotFoundException(nameof(project), projectId);

            _projectRepository.Delete(project);
            _context.SaveChanges();
        }

        public void Update(UpdateProjectDTO newProject)
        {
            var project = _projectRepository.GetById(newProject.Id);

            if (project is null) throw new NotFoundException(nameof(project), newProject.Id);

            _mapper.Map(newProject, project);
            project.UpdatedAt = DateTime.Now;

            _projectRepository.Update(project);
            _context.SaveChanges();
        }

        // Query 1
        public ICollection<ProjectTasksNumberDTO> GetUserProjectsTasksNumber(int userId)
        {
            return _projectRepository.GetAll(p => p.AuthorId == userId, true, s => s.Include(p => p.Tasks))
                .Select(project => new ProjectTasksNumberDTO { Project = _mapper.Map<ProjectDTO>(project), TasksNumber = project.Tasks.Count })
                .ToList();
        }

        // Query 7
        public ICollection<ProjectStaticticsDTO> GetProjectStatictics(int minProjectDescriptionLengthForCounting, int maxProjectTasksCountForCounting)
        {
            return _projectRepository.GetAll(null, true, 
                    s => s.Include(p => p.Tasks)
                        .Include(p => p.Team)
                        .ThenInclude(t => t.Users))
            .Select(project => new ProjectStaticticsDTO
            {
                Project = _mapper.Map<ProjectDTO>(project),
                LongestTaskByDescription = project.Tasks.Any()
                           ? _mapper.Map<TaskDTO>(project.Tasks.Aggregate((t1, t2) => t1.Description.Length > t2.Description.Length ? t1 : t2))
                           : null,
                ShortestTaskByName = project.Tasks.Any()
                           ? _mapper.Map<TaskDTO>(project.Tasks.Aggregate((t1, t2) => t1.Name.Length > t2.Name.Length ? t2 : t1))
                           : null,
                UsersNumber = project.Description.Length > minProjectDescriptionLengthForCounting
                       || project.Tasks.Count < maxProjectTasksCountForCounting
                       ? project.Team.Users.Count
                       : null
            })
            .ToList();
        }
    }
}
