﻿using AutoMapper;
using ProjectsEF.BLL.Services.Abstract;
using ProjectsEF.DAL.Entities;
using ProjectsEF.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using ProjectsEF.Common.DTOs.Team;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ProjectsEF.BLL.Exceptions;

namespace ProjectsEF.BLL.Services
{
    public class TeamsService : BaseService, ITeamsService
    {
        private readonly IUnitOfWork _context;
        private readonly IRepository<Team> _teamRepository;

        public TeamsService(IMapper mapper, IUnitOfWork context) : base(mapper)
        {
            _context = context;
            _teamRepository = _context.TeamRepository;
        }

        public IEnumerable<TeamDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(_teamRepository.GetAll());
        }

        public TeamDTO Get(int teamId)
        {
            var team = _teamRepository.GetById(teamId);

            if (team is null) throw new NotFoundException(nameof(team), teamId);

            return _mapper.Map<TeamDTO>(team);
        }

        public TeamDTO Add(NewTeamDTO team)
        {
            var newTeam = _teamRepository.Add(_mapper.Map<Team>(team));
            _context.SaveChanges();

            return _mapper.Map<TeamDTO>(newTeam);
        }

        public void Delete(int teamId)
        {
            var team = _teamRepository.GetById(teamId);

            if (team is null) throw new NotFoundException(nameof(team), teamId);

            _teamRepository.Delete(team);
            _context.SaveChanges();
        }

        public void Update(UpdateTeamDTO newTeam)
        {
            var team = _teamRepository.GetById(newTeam.Id);

            if (team is null) throw new NotFoundException(nameof(team), newTeam.Id);

            _mapper.Map(newTeam, team);
            team.UpdatedAt = DateTime.Now;

            _teamRepository.Update(team);
            _context.SaveChanges();
        }

        // Query 4
        public ICollection<TeamUsersDTO> GetTeamUsersOlderThan(int minYearsNumber)
        {
            var currentYear = DateTime.Now.Year;

            return _mapper.Map<ICollection<TeamUsersDTO>>(
                _teamRepository.GetAll(
                    t => t.Users.Any() && !t.Users.Any(u => currentYear - u.BirthDay.Year < minYearsNumber), 
                    true, s => s.Include(p => p.Users))
                );
        }
    }
}
