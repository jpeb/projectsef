﻿using ProjectsEF.Common.DTOs.User;
using System.Collections.Generic;

namespace ProjectsEF.BLL.Services.Abstract
{
    public interface IUsersService
    {
        IEnumerable<UserDTO> GetAll();
        UserDTO Get(int userId);
        void Update(UpdateUserDTO user);
        UserDTO Add(NewUserDTO user);
        void Delete(int userId);

        // Query 5
        ICollection<UserTasksDTO> GetUsersWithTasks();

        // Query 6
        UserTaskStaticticsDTO GetUserTaskStatictics(int userId);
    }
}
