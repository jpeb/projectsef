﻿using ProjectsEF.Common.DTOs.Project;
using System.Collections.Generic;

namespace ProjectsEF.BLL.Services.Abstract
{
    public interface IProjectsService
    {
        IEnumerable<ProjectDTO> GetAll();
        ProjectDTO Get(int projectId);
        void Update(UpdateProjectDTO project);
        ProjectDTO Add(NewProjectDTO project);
        void Delete(int projectId);

        // Query 1
        ICollection<ProjectTasksNumberDTO> GetUserProjectsTasksNumber(int userId);

        // Query 7
        ICollection<ProjectStaticticsDTO> GetProjectStatictics(int minProjectDescriptionLengthForCounting, int maxProjectTasksCountForCounting);
    }
}
