﻿using ProjectsEF.Common.DTOs.Task;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectsEF.BLL.Services.Abstract
{
    public interface ITasksService
    {
        IEnumerable<TaskDTO> GetAll();
        TaskDTO Get(int taskId);
        void Update(UpdateTaskDTO task);
        TaskDTO Add(NewTaskDTO task);
        void Delete(int taskId);

        // Query 2
        ICollection<TaskDTO> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength);
        // Query 3
        ICollection<TaskNameDTO> GetUserTasksFinishedInYear(int userId, int finishedYear);
    }
}
