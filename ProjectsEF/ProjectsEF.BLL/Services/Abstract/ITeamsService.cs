﻿using ProjectsEF.Common.DTOs.Team;
using System;
using System.Collections.Generic;

namespace ProjectsEF.BLL.Services.Abstract
{
    public interface ITeamsService
    {
        IEnumerable<TeamDTO> GetAll();
        TeamDTO Get(int teamId);
        void Update(UpdateTeamDTO team);
        TeamDTO Add(NewTeamDTO team);
        void Delete(int teamId);
        
        // Query 4
        ICollection<TeamUsersDTO> GetTeamUsersOlderThan(int minYearsNumber);
    }
}
