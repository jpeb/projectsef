﻿using AutoMapper;
using ProjectsEF.Common.DTOs.Task;
using ProjectsEF.BLL.Services.Abstract;
using ProjectsEF.DAL.Entities;
using ProjectsEF.DAL.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Task = ProjectsEF.DAL.Entities.Task;
using ProjectsEF.BLL.Exceptions;

namespace ProjectsEF.BLL.Services
{
    public class TasksService : BaseService, ITasksService
    {
        private readonly IUnitOfWork _context;
        private readonly IRepository<Task> _taskRepository;

        public TasksService(IMapper mapper, IUnitOfWork context) : base(mapper)
        {
            _context = context;
            _taskRepository = _context.TaskRepository;
        }

        public IEnumerable<TaskDTO> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(_taskRepository.GetAll());
        }

        public TaskDTO Get(int taskId)
        {
            var task = _taskRepository.GetById(taskId);

            if (task is null) throw new NotFoundException(nameof(task), taskId);

            return _mapper.Map<TaskDTO>(task);
        }

        public TaskDTO Add(NewTaskDTO task)
        {
            var newTask = _taskRepository.Add(_mapper.Map<Task>(task));
            _context.SaveChanges();

            return _mapper.Map<TaskDTO>(newTask);
        }

        public void Delete(int taskId)
        {
            var task = _taskRepository.GetById(taskId);

            if (task is null) throw new NotFoundException(nameof(task), taskId);

            _taskRepository.Delete(task);
            _context.SaveChanges();
        }

        public void Update(UpdateTaskDTO newTask)
        {
            var task = _taskRepository.GetById(newTask.Id);

            if (task is null) throw new NotFoundException(nameof(task), newTask.Id);

            _mapper.Map(newTask, task);
            task.UpdatedAt = DateTime.Now;

            _taskRepository.Update(task);
            _context.SaveChanges();
        }

        // Query 2
        public ICollection<TaskDTO> GetUserTasksWithNameLessThen(int userId, int maxTaskNameLength)
        {
            var tasks = _taskRepository
                    .GetAll(t => t.PerformerId == userId && t.Name.Length < maxTaskNameLength);

            return _mapper.Map<ICollection<TaskDTO>>(tasks);
        }

        // Query 3
        public ICollection<TaskNameDTO> GetUserTasksFinishedInYear(int userId, int finishedYear)
        {
            var tasks = _taskRepository
                .GetAll(t => t.PerformerId == userId &&
                                t.FinishedAt.HasValue &&
                                t.FinishedAt.Value.Year == finishedYear);

            return _mapper.Map<ICollection<TaskNameDTO>>(tasks);
        }
    }
}
